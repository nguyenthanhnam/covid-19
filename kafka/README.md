Run the application and use curl to send some data:

`$ curl -X POST http://localhost:8080/send/sample`

and body content:

`{"name":"Nam Nguyen","address":"HCM city"}`