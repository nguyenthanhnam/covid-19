package xyz.ncov.kafka.common;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
public class CovidSample implements Serializable {
    private Person doubtPerson;
}