package xyz.ncov.kafka.common;

import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
public class PositiveData {

    private int count;
    private List<Person> infectedPersons = new ArrayList<>();

    @Override
    public String toString() {
        return "PositiveData{" +
                "infectedPersons=" + infectedPersons +
                '}';
    }
}
