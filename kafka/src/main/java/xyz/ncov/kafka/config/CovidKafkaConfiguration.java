package xyz.ncov.kafka.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import xyz.ncov.kafka.common.PositiveData;
import xyz.ncov.kafka.lab.PasteurLab;
import xyz.ncov.kafka.resolver.MedicalAnnouncement;

import java.util.PriorityQueue;

@Configuration
public class CovidKafkaConfiguration {

    @Bean
    public PositiveData positiveData() {
        return new PositiveData();
    }

    @Bean
    public NewTopic topicPositive() {
        return TopicBuilder.name("topic.Positive").partitions(1).replicas(1).build();
    }

    @Bean
    public NewTopic topicSample() {
        return TopicBuilder.name("topic.Sample").partitions(1).replicas(1).build();
    }

    @Bean
    public MedicalAnnouncement medicalAnnouncement(PositiveData positiveData) {
        return new MedicalAnnouncement(positiveData);
    }

    @Bean
    public PasteurLab pasteurLab() {
        return new PasteurLab(new PriorityQueue());
    }
}
