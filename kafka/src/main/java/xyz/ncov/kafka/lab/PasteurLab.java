package xyz.ncov.kafka.lab;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import xyz.ncov.kafka.common.CovidSample;
import xyz.ncov.kafka.common.Person;

import java.util.Queue;
import java.util.Random;

public class PasteurLab {

    @Autowired
    private KafkaTemplate<String, Person> kafkaTemplate;

    private Queue<CovidSample> covidSampleQueue;

    public PasteurLab(Queue sampleQueue) {
        this.covidSampleQueue = sampleQueue;
    }

    @KafkaListener(id = "sampleGroup", topics = "topic.Sample")
    public void receive(CovidSample covidSample) {
        covidSampleQueue.add(covidSample);
        run();
    }

    private boolean process(CovidSample covidSample) {
        return new Random().nextBoolean();
    }

    public void run() {
        CovidSample covidSample = covidSampleQueue.poll();
        if (covidSample != null) {
            if (process(covidSample)) {
                kafkaTemplate.send("topic.Positive", covidSample.getDoubtPerson());
            }
        }
    }
}
