package xyz.ncov.kafka.start;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.ImportAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import xyz.ncov.kafka.config.CovidKafkaConfiguration;
import xyz.ncov.kafka.resolver.CDC;

@ImportAutoConfiguration(classes = {CovidKafkaConfiguration.class})
@SpringBootApplication
@ComponentScan(basePackageClasses = CDC.class)
public class StartApp {
    public static void main(String[] args) {
        SpringApplication.run(StartApp.class, args);
    }
}
