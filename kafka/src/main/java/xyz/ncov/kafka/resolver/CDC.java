package xyz.ncov.kafka.resolver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.*;
import xyz.ncov.kafka.common.Person;
import xyz.ncov.kafka.common.CovidSample;

import java.util.Arrays;

@RestController
public class CDC {

    @Autowired
    private KafkaTemplate<String, CovidSample> kafkaTemplate;

    private CovidSample makeCovidSample(Person doubtPerson) {
        CovidSample covidSample = new CovidSample();
        Person contactPerson = new Person();
        contactPerson.setName("Another My Wife");
        contactPerson.setAddress("HCM city");
        doubtPerson.setContactedPersons(Arrays.asList(contactPerson));
        covidSample.setDoubtPerson(doubtPerson);
        return covidSample;
    }

    @PostMapping(path = "/send/sample")
    public void send(@RequestBody Person person) {
        kafkaTemplate.send("topic.Sample", makeCovidSample(person));
    }

    @KafkaListener(id = "positiveGroup2", topics = "topic.Positive")
    public void receive(Person newInfectedPerson) {
        for (Person contactedPerson : newInfectedPerson.getContactedPersons()) {
            CovidSample covidSample = makeCovidSample(contactedPerson);
            System.out.println("CDC prepares new case:" + contactedPerson.getName());
            kafkaTemplate.send("topic.Sample", covidSample);
        }
    }
}
