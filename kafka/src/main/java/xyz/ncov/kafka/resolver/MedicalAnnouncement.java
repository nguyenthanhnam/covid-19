package xyz.ncov.kafka.resolver;

import lombok.AllArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import xyz.ncov.kafka.common.Person;
import xyz.ncov.kafka.common.PositiveData;

@AllArgsConstructor
public class MedicalAnnouncement {

    private PositiveData positiveData;

    @KafkaListener(id = "positiveGroup1", topics = "topic.Positive")
    public void receive(Person infectedPerson) {
        positiveData.setCount(positiveData.getCount() + 1);
        positiveData.getInfectedPersons().add(infectedPerson);
        announce(1);
    }

    private void announce(int newCaseCount) {
        System.out.println(positiveData.getCount() + " with new cases: " + newCaseCount);
        System.out.println("Infected Person List: " + positiveData.getInfectedPersons().toString());

    }
}