package xyz.ncov.batch.listener;

import lombok.AllArgsConstructor;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import xyz.ncov.batch.entity.CoronaCases;
import xyz.ncov.batch.repository.CoronaCasesRepository;

import java.util.List;

@AllArgsConstructor
public class UpdateCoronaJobExecutionListener extends JobExecutionListenerSupport {

    private CoronaCasesRepository coronaCasesRepository;

    @Override
    public void afterJob(JobExecution jobExecution) {
        if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
            List<CoronaCases> coronaCasesList = coronaCasesRepository.findAll();
            for (CoronaCases coronaCases : coronaCasesList) {
                System.out.println(coronaCases);
            }
        }
    }
}
