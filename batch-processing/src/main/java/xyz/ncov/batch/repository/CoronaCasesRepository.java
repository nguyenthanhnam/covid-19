package xyz.ncov.batch.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import xyz.ncov.batch.entity.CoronaCases;

import java.util.List;

@Repository
public interface CoronaCasesRepository extends JpaRepository<CoronaCases, Integer> {

    public CoronaCases findByCountry(String country);
}
