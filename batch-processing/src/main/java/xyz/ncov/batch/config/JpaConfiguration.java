package xyz.ncov.batch.config;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import xyz.ncov.batch.entity.CoronaCases;

@EnableJpaRepositories({"xyz.ncov.batch.repository"})
@EntityScan(basePackageClasses = {CoronaCases.class})
public class JpaConfiguration {
}
