package xyz.ncov.batch.config;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobExecutionListener;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.item.ItemProcessor;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.ItemWriter;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.Resource;
import xyz.ncov.batch.entity.CoronaCases;
import xyz.ncov.batch.listener.UpdateCoronaJobExecutionListener;
import xyz.ncov.batch.model.CoronaRecord;
import xyz.ncov.batch.processor.CoronaRecordItemProcessor;
import xyz.ncov.batch.reader.CoronaRecordFieldSetMapper;
import xyz.ncov.batch.repository.CoronaCasesRepository;
import xyz.ncov.batch.writer.CoronaCasesItemWriter;

import java.beans.PropertyEditor;
import java.text.SimpleDateFormat;

@Configuration
@EnableBatchProcessing
public class BatchConfiguration {

    @Autowired
    private StepBuilderFactory stepBuilderFactory;

    @Autowired
    private JobBuilderFactory jobBuilderFactory;

    @Value("classpath:input/corona-record.csv")
    private Resource resource;

    @Bean
    public ItemWriter<CoronaCases> itemWriter(CoronaCasesRepository coronaCasesRepository) {
        return new CoronaCasesItemWriter(coronaCasesRepository);
    }

    private PropertyEditor dateEditor() {
        return new CustomDateEditor(new SimpleDateFormat("dd/MM/yyyy"), true);
    }

    @Bean
    public ItemReader<CoronaRecord> itemReader() {
        return new FlatFileItemReaderBuilder<CoronaRecord>().name("coronaRecordItemReader")
                .resource(resource)
                .delimited()
                .names(new String[] {"Country", "New Cases", "New Deaths", "New Recovered", "Date"})
                .linesToSkip(1)
                .fieldSetMapper(new CoronaRecordFieldSetMapper())
                .build();
    }

    @Bean
    public ItemProcessor<CoronaRecord, CoronaCases> itemProcessor(CoronaCasesRepository coronaCasesRepository) {
        return new CoronaRecordItemProcessor(coronaCasesRepository);
    }

    @Bean
    public Step step1(ItemReader<CoronaRecord> itemReader, ItemProcessor<CoronaRecord, CoronaCases> itemProcessor
            , ItemWriter<CoronaCases> itemWriter) {
        return stepBuilderFactory.get("step1")
                .<CoronaRecord, CoronaCases>chunk(3)
                .reader(itemReader)
                .processor(itemProcessor)
                .writer(itemWriter)
                .build();
    }

    @Bean
    public JobExecutionListener updateCoronaJobExecutionListener(CoronaCasesRepository coronaCasesRepository) {
        return new UpdateCoronaJobExecutionListener(coronaCasesRepository);
    }

    @Bean
    public Job updateCoronaJob(Step step1, JobExecutionListener updateCoronaJobExecutionListener) {
        return jobBuilderFactory.get("updateCoronaJob").flow(step1).end().listener(updateCoronaJobExecutionListener).build();
    }
}
