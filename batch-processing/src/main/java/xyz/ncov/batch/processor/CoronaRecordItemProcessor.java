package xyz.ncov.batch.processor;

import lombok.AllArgsConstructor;
import org.springframework.batch.item.ItemProcessor;
import xyz.ncov.batch.entity.CoronaCases;
import xyz.ncov.batch.model.CoronaRecord;
import xyz.ncov.batch.repository.CoronaCasesRepository;

import java.time.LocalDateTime;

@AllArgsConstructor
public class CoronaRecordItemProcessor implements ItemProcessor<CoronaRecord, CoronaCases> {

    private CoronaCasesRepository coronaCasesRepository;

    @Override
    public CoronaCases process(CoronaRecord coronaRecord) throws Exception {
        CoronaCases coronaCases = coronaCasesRepository.findByCountry(coronaRecord.getCountry());
        coronaCases.setTotalCases(coronaCases.getTotalCases() + coronaRecord.getNewCases());
        coronaCases.setTotalDeaths(coronaCases.getTotalDeaths() + coronaRecord.getNewDeaths());
        coronaCases.setTotalRecovered(coronaCases.getTotalRecovered() + coronaRecord.getNewRecovered());
        coronaCases.setUpdateDate(LocalDateTime.now());
        return coronaCases;
    }
}
