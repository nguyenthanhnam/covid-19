package xyz.ncov.batch.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class CoronaRecord {

    private String country;
    private Integer newCases;
    private Integer newDeaths;
    private Integer newRecovered;
    private LocalDate date;
}
