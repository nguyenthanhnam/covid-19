package xyz.ncov.batch.writer;

import lombok.AllArgsConstructor;
import org.springframework.batch.item.ItemWriter;
import xyz.ncov.batch.entity.CoronaCases;
import xyz.ncov.batch.repository.CoronaCasesRepository;

import java.util.List;

@AllArgsConstructor
public class CoronaCasesItemWriter implements ItemWriter<CoronaCases> {

    private CoronaCasesRepository coronaCasesRepository;

    @Override
    public void write(List<? extends CoronaCases> items) throws Exception {
        for (CoronaCases item : items) {
            coronaCasesRepository.save(item);
        }
    }
}
