package xyz.ncov.batch.reader;

import org.springframework.batch.item.file.mapping.FieldSetMapper;
import org.springframework.batch.item.file.transform.FieldSet;
import org.springframework.validation.BindException;
import xyz.ncov.batch.model.CoronaRecord;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class CoronaRecordFieldSetMapper implements FieldSetMapper<CoronaRecord> {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    @Override
    public CoronaRecord mapFieldSet(FieldSet fieldSet) throws BindException {
        CoronaRecord coronaRecord = new CoronaRecord();
        coronaRecord.setCountry(fieldSet.readString("Country"));
        coronaRecord.setNewCases(fieldSet.readInt("New Cases"));
        coronaRecord.setNewDeaths(fieldSet.readInt("New Deaths"));
        coronaRecord.setNewRecovered(fieldSet.readInt("New Recovered"));
        String date = fieldSet.readString("Date");
        coronaRecord.setDate(LocalDate.parse(date, DATE_TIME_FORMATTER));
        return coronaRecord;
    }
}
