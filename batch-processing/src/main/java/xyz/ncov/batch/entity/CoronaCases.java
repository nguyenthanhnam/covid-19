package xyz.ncov.batch.entity;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.time.LocalDateTime;

@Getter
@Setter
@ToString
@Entity(name = "CORONA_CASES")
public class CoronaCases {

    @Id
    private Integer id;
    private String country;
    private Integer totalCases;
    private Integer totalDeaths;
    private Integer totalRecovered;
    private LocalDateTime updateDate;

}

