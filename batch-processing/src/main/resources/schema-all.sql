CREATE TABLE CORONA_CASES (
	id int not null primary key,
	country varchar(50) not null,
	total_cases int not null,
	total_deaths int not null,
	total_recovered int not null,
	update_date timestamp default CURRENT_TIMESTAMP
);

